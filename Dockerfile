FROM alpine:latest

RUN apk update && apk add git git-fast-import openssh python2 python3 mercurial python2-dev py2-pip gcc musl-dev
RUN mkdir /opt/hgclone
WORKDIR /opt/hgclone

RUN git clone https://bitbucket.org/edrandall/bitbucket-hg-to-git.git
RUN git clone https://github.com/felipec/git-remote-hg.git
ENV PATH=${PATH}:/opt/hgclone/git-remote-hg:/opt/hgclone/bitbucket-hg-to-git/src
RUN chmod +x /opt/hgclone/bitbucket-hg-to-git/src/bbhg2git.py
RUN pip install mercurial

COPY id_rsa /root/.ssh/id_rsa